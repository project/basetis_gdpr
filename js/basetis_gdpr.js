function update_text() {
  var checkedValue= '';
  var inputElements = jQuery('.form-checkbox:checked');
  var firstInput = 0;
  var modal_messages = Drupal.settings.basetis_gdpr.modal_messages;

  for(var i=0; inputElements[i]; ++i){
    var id= inputElements[i].value;
    var name = '';
    if(!isNaN(id)){
      name = jQuery('#label_'+id)[0].value;
      if(firstInput == 0) {
        checkedValue += name;
        firstInput = 1;
      } else if (i==inputElements.length-1) {
        checkedValue += ' '+modal_messages['and']+' '+name;
      } else {
        checkedValue += ', '+name;
      }
    }
  }
  if(checkedValue != '') {
  	jQuery('#button-accept-delete').removeClass('disabled');
  	jQuery('#text_ad').text(modal_messages['itemText1']);
    jQuery('#checkbox_values').text( modal_messages['itemText2']+' ' + checkedValue);
  }
  else {
  	jQuery('#button-accept-delete').addClass('disabled');
  	jQuery('#text_ad').text(modal_messages['noItem']);
    jQuery('#checkbox_values').text('');
  }
modal();
}


function modal() {
jQuery('#delete_modal').fadeToggle();
}

window.onclick = function(event) {
  if (event.target == document.getElementById('delete_modal')) {
    modal();
  }
}