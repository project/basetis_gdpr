<?php

/**
 * Sensitive pages crud.
 */

/**
 * Depending on the part of the page selected a different form will be used.
 */
function basetis_gdpr_form($form, &$form_state) {
  $arg_3 = arg(3);
  $arg_4 = arg(4);
  if (isset($arg_3) and ($arg_3 == 'add')) {
    $form = basetis_gdpr_pages_insert_list($form, $form_state);
  }
  elseif (isset($arg_3) and isset($arg_4) and is_numeric($arg_3) and ($arg_4 == 'edit')) {
    $form = basetis_gdpr_pages_edit_list($form, $form_state);
  }
  else {
    $form = basetis_gdpr_pages_list($form, $form_state);
  }
  return $form;
}

/**
 * Prepairs an array with levels names.
 */
function basetis_gdpr_list_levels() {
  $levels = basetis_gdpr_get_table_levels();
  $name_levels = array();
  $name_levels[] = '';
  foreach ($levels as $key) {
    $name_levels[$key->id] = $key->level;
  }
  return $name_levels;
}

/**
 * Shows the sensitive pages table and lets you edit, insert, delete and filter.
 */
function basetis_gdpr_pages_list($form, $form_state) {
  drupal_add_js(drupal_get_path('module', 'basetis_gdpr') . '/js/basetis_gdpr.js');
  drupal_add_css('https://use.fontawesome.com/releases/v5.4.2/css/all.css', 'external');
  drupal_add_css(drupal_get_path('module', 'basetis_gdpr') . '/css/basetis_gdpr_delete.css');

  $form['insert']['start_div'] = array(
    '#markup' => '<div>',
  );

  $url_to_go = url('admin/gdpr/sensitive_pages/add');

  $form['insert']['insert_button'] = array(
    '#markup' => '<a class="button" class="block_elem"  href="' . $url_to_go . '" style="padding-right: 10px;">' . t('Insert data') . '<i class="fa fa-plus" style="margin-left: 10px;"></i> </a>',
  );

  $form['insert']['text_2'] = array(
    '#markup' => '<p id="text_2" class="block_elem">' . t("This page displays which paths of your website have sensitive data.") . '
    <br> ' . t("You can insert new pages, edit pages that already exist or delete some of them.") . '</p>',
  );

  $form['insert']['end_div'] = array(
    '#markup' => '</div>',
  );

  $tableData = basetis_gdpr_get_table_data();
  $tableHeader = $tableData['tableHeader'];
  $tableRows = $tableData['tableRows'];
  $nom_levels = basetis_gdpr_list_levels();

  $form['filtre']['start_div'] = array(
    '#markup' => '<div class= "inicio_filtro">',
  );
  $form['filtre']['text_1'] = array(
    '#markup' => '<div  style="
    font-size: 0.923em;
    margin: 0;
    padding-bottom: 9px;
    font-weight: bold;">' . t("FILTER") . '</div>',
  );

  $form['filtre']['path'] = array(
    '#prefix' => '<div class="filtro_tabla">',
    '#type'          => 'textfield',
    '#title'         => t('Path'),
    '#default_value' => isset($_SESSION['basetis_gdpr_filtre']['path']) ? $_SESSION['basetis_gdpr_filtre']['path'] : '',
    '#size'          => 15,
    '#maxlength'     => 15,
    '#suffix' => '</div>',
  );

  $form['filtre']['level'] = array(
    '#prefix' => '<div class="filtro_tabla">',
    '#type'          => 'select',
    '#title'         => t('Level'),
    '#options' => $nom_levels,
    '#default_value' => isset($_SESSION['basetis_gdpr_filtre']['level']) ? $_SESSION['basetis_gdpr_filtre']['level'] : '',
    '#suffix' => '</div>',
  );

  $form['filtre']['submit'] = array(
    '#prefix' => '<div class="filtro_tabla">',
    '#type'     => 'submit',
    '#value'    => t('Find'),
    '#submit'    => array('basetis_gdpr_submit_buscar_submit'),
    '#suffix' => '</div>',
  );

  $form['filtre']['end_div'] = array(
    '#markup' => '</div>',
  );

  $form['tabla'] = array(
    '#prefix' => '<div>',
    '#type' => 'tableselect',
    '#header' => $tableHeader,
    '#options' => $tableRows,
    '#empty' => t('No content available.'),
    '#multiple' => TRUE,
    '#sufix' => '</div>',
  );

  $form['pager'] = array('#markup' => theme('pager'));

  $form['boton_delete'] = array(
    '#markup' => '<a class="button" class="report-item" onclick="javascript:update_text()">' . t('Delete selected records') . '<i class="far fa-trash-alt" style="margin-left: 10px;"></i> </a>',
  );

  $modal_messages = array(
    'noItem' => t('There is no item to delete'),
    'itemText1' => t('Are you sure?'),
    'itemText2' => t('The next items are going to be deleted:'),
    'and'       => t('and'),
  );

  drupal_add_js(array('basetis_gdpr' => array('modal_messages' => $modal_messages)), 'setting');

  $form['delete_rows'] = array(
    '#prefix' => '<div id="delete_modal" class="delete_modal" style="display: none">
                    <!-- Modal content -->
                    <div class="delete_modal-content">
                      <div class="delete_modal-header">
                      </div>
                      <div class="delete_modal-body">
                         <h2><i class="fa fa-exclamation-triangle fa-2x" style="color: #d45867;"></i></h2>
                        <p id="text_ad" style="font-weight: bold;">Are you sure?</p>
                        <p id="checkbox_values"></p>
                      </div>
                      <div class="delete_modal-footer">
                        <a class="button button-cancel" class="report-item" onclick="javascript:modal()">' . t('Cancel') . '</a>',
    '#type'   => 'submit',
    '#value'  => t('Delete'),
    '#submit' => array('basetis_gdpr_submit_borrar_submit'),
    '#attributes' => array(
      'class' => array('btn btn-danger pull-right'),
      'id' => 'button-accept-delete',
    ),
    '#sufix'  => '    </div>
                    </div>
                  </div>',
  );

  return $form;
}

/**
 * You can insert data from this form.
 */
function basetis_gdpr_pages_insert_list($form, &$form_state) {
  drupal_add_css('https://use.fontawesome.com/releases/v5.4.2/css/all.css', 'external');

  $link_select = url('/admin/gdpr/sensitive_pages');

  $form['back_to_table'] = array(
    '#markup' => '<div><a href="' . $link_select . '" class="form-submit"><i class="fa fa-arrow-circle-left"></i> ' . t('Go back') . '</a></div>',
  );

  $form['text_3'] = array(
    '#markup' => '<p id="text_3"> ' . t("This page offers the possibility to add a sensitive level to a page that doesn't have one yet.") . '</p>',
  );

  $form['path'] = array(
    '#type' => 'textfield',
    '#title' => t('Path'),
    '#required' => TRUE,
    '#description' => t('Add the path of the page on which the level will be applied'),
  );

  $form['description'] = array(
    '#type' => 'textfield',
    '#title' => t('Description'),
    '#description' => t('Add the description of the page on which the level will be applied'),
  );

  $nom_levels = basetis_gdpr_list_levels();

  $form['level'] = array(
    '#prefix' => '<div class="filtro_tabla">',
    '#type'          => 'select',
    '#title'         => t('Level'),
    '#options' => $nom_levels,
    '#default_value' => '1',
    '#description' => t('Select the sensitive level that will be applied to the page'),
    '#suffix' => '</div>',
    '#required' => TRUE,
  );

  $form['insertar'] = array(
    '#prefix' => '<div>',
    '#type' => 'submit',
    '#value' => t('Save'),
    '#sufix' => '</div>',
  );

  return $form;
}

/**
 * Form that lets you edit data.
 */
function basetis_gdpr_pages_edit_list($form, &$form_state) {
  drupal_add_css('https://use.fontawesome.com/releases/v5.4.2/css/all.css', 'external');

  $link_select = url('admin/gdpr/sensitive_pages/');

  $form['back_to_table'] = array(
    '#markup' => '<div><a href="' . $link_select . '" class="form-submit"><i class="fa fa-arrow-circle-left"></i> ' . t('Go back') . '</a></div>',
  );
  $form['text_4'] = array(
    '#markup' => '<p id="text_4"> ' . t("This page offers the possibility to edit the selected row of the table: modify the path if it has changed or give another description or level to the page.") . '</p>',
  );

  $argument = arg(3);
  $params = array(
    'edit_id'  => isset($argument) ? $argument : NULL,
  );
  $sol = basetis_gdpr_get_table_records($form, $params);
  $path = '';
  $description = '';
  $level = '';
  foreach ($sol as $rowdata) {
    $path = $rowdata->path;
    $description = $rowdata->description;
    $level = $rowdata->level;
  }

  $form['path'] = array(
    '#type' => 'textfield',
    '#title' => t('Path'),
    '#default_value' => $path,
    '#description' => t('If you modify the path remember there should not exist another page with this path in the table.'),
    '#required' => TRUE,
  );

  $form['description'] = array(
    '#type' => 'textfield',
    '#title' => t('Description'),
    '#description' => t('This page offers the possibility to change the page description or delte it, otherwise it will stay the same.'),
    '#default_value' => $description,
  );

  $nom_levels = basetis_gdpr_list_levels();

  $form['level'] = array(
    '#prefix' => '<div class="filtro_tabla">',
    '#type'          => 'select',
    '#title'         => t('Level'),
    '#options' => $nom_levels,
    '#default_value' => isset($level) ? $level : '',
    '#description' => t('This page offers the possibility to select a diferent senstitive level, otherwise it will stay the same.'),
    '#suffix' => '</div>',
    '#required' => TRUE,
  );

  $form['edit'] = array(
    '#prefix' => '<div>',
    '#type' => 'submit',
    '#value' => t('Save'),
    '#sufix' => '</div>',
  );

  return $form;
}

/**
 * Inserts values of path, description and level.
 *
 * Used in basetis_gdpr_form_submit.
 */
function basetis_gdpr_insert($form) {
  $path = $form['path']['#value'];
  $path = trim($path);
  $description = $form['description']['#value'];
  $level = $form['level']['#value'];
  $create_date = date('Y-m-d H:i:s');
  $update_date = date('Y-m-d H:i:s');
  try {
    db_insert('{basetis_gdpr_sensitive_pages}')
      ->fields(array(
        'path'          => $path,
        'description'   => $description,
        'level'         => $level,
        'creation_date' => $create_date,
        'update_date'   => $update_date,
      ))
    ->execute();
  }
  catch (Exception $e) {
    return FALSE;
  }
  return TRUE;
}

/**
 * Edits the parameters of a sensitive_page.
 *
 * Used in basetis_gdpr_form_submit */
function basetis_gdpr_edit($form) {
  $path = $form['path']['#value'];
  $path = trim($path);
  $description = $form['description']['#value'];
  $level = $form['level']['#value'];
  $update_date = date('Y-m-d H:i:s');
  $id = arg(3);
  try {
    db_update('{basetis_gdpr_sensitive_pages}')
      ->fields(array(
        'path'          => $path,
        'description'   => $description,
        'level'         => $level,
        'update_date'   => $update_date,
      ))
      ->condition('id',$id, '=')
      ->execute();
  }
  catch (Exception $e) {
    return FALSE;
  }
  return TRUE;
}

/**
 * Deletes data from database.
 */
function basetis_gdpr_delete($form, $ids) {
  try {
    db_delete('{basetis_gdpr_sensitive_pages}')
      ->condition('id', $ids, 'IN')
      ->execute();
  }
  catch (Exception $e) {
    return FALSE;
  }
}

/**
 * Gives values to params.
 */
function basetis_gdpr_get_table_params() {
  $params = array(
    'filtre_ruta'  => isset($_SESSION['basetis_gdpr_filtre']['path']) ? $_SESSION['basetis_gdpr_filtre']['path'] : NULL,
    'filtre_level'    => isset($_SESSION['basetis_gdpr_filtre']['level']) ? $_SESSION['basetis_gdpr_filtre']['level'] : NULL,
  );
  return $params;
}

/**
 * The structure of the table is created.
 */
function basetis_gdpr_get_table_data() {
  $tableHeader = array('id',
    'path',
    'description',
    'level',
    'creation_date',
    'update_date',
    'edit'
  );
  $tableHeader2 = array(
    'path_name'     => array('data' => t('Page path'), 'field' => 'url'),
    'description'   => array('data' => t('Page description'), 'field' => 'description'),
    'level'         => array('data' => t('Page sensitive level'), 'field' => 'level'),
    'creation_date' => array('data' => t('Register creation date'), 'field' => 'session_date'),
    'update_date'   => array('data' => t('Register update date'), 'field' => 'update_date'),
    'edit'          => array('data' => t('Edit')),
  );

  $params = basetis_gdpr_get_table_params();
  $data = basetis_gdpr_get_table_records($tableHeader, $params);
  $tableRows = array();

  foreach ($data as $rowdata) {
    $edit = '<a href="' . url('admin/gdpr/sensitive_pages/' . $rowdata->id) . '/edit"><i class="fa fa-edit"></i></a>';
    $sensitive_level = check_plain($rowdata->l_level);
    $page_path = check_plain($rowdata->path);
    $page_description = check_plain($rowdata->description);
    $tableRows[$rowdata->id] = array(
      'id'           => $rowdata->id,
      'path_name'    => '<data value="' . $page_path . '" id="label_' . $rowdata->id . '">' . $page_path . '</data>',
      'path'         => $page_path,
      'description'  => $page_description,
      'level'        => $sensitive_level,
      'creation_date'  => $rowdata->creation_date,
      'update_date'    => $rowdata->update_date,
      'edit'           => $edit,
    );
  }

  $returnTable = array(
    'tableHeader' => $tableHeader2,
    'tableRows' => $tableRows,
  );

  return $returnTable;
}

/**
 * Data to show is being taken of the table "basetis_gdpr_sensitive_pages".
 */
function basetis_gdpr_get_table_records($tableHeader, $params) {
  $query = db_select('basetis_gdpr_sensitive_pages', 'p')
    ->extend('PagerDefault')
    ->limit(15)
    ->extend('TableSort')
    ->orderByHeader($tableHeader);
  $query->leftJoin('basetis_gdpr_page_levels', 'l', 'p.level = l.id');

  $array_fields = array('id',
    'path',
    'description',
    'level',
    'creation_date',
    'update_date'
  );

  $query->fields('p', $array_fields);
  $query->fields('l', array('level'));

  if (isset($params['filtre_ruta']) and $params['filtre_ruta'] != '') {
    $query->condition('p.path', $params['filtre_ruta'] . '%', 'LIKE');
  }

  if (isset($params['filtre_level']) and $params['filtre_level'] != '0') {
    $query->condition('p.level', $params['filtre_level'], '=');
  }

  if (isset($params['edit_id']) and is_numeric($params['edit_id'])) {
    $query->condition('p.id', $params['edit_id'], '=');
  }
  try {
    $results = $query->execute();
  }
  catch (Exception $e) {
    return FALSE;
  }

  return $results;
}

function basetis_gdpr_get_table_levels() {
  $query = db_select('basetis_gdpr_page_levels', 'l')
    ->distinct()
    ->extend('PagerDefault')
    ->extend('TableSort');
  $query->fields('l', array('id', 'level'));

  try {
    $results = $query->execute();
  }
  catch (Exception $e) {
    return FALSE;
  }

  return $results;
}

/**
 * Gets the paths of sensitive levels.
 */
function basetis_gdpr_validation_names() {
  $query = db_select('basetis_gdpr_sensitive_pages', 's')
    ->fields('s', array('path'));
  try {
    $results = $query->execute();
  }
  catch (Exception $e) {
    return FALSE;
  }
  return $results;
}

/**
 * General validations of data before saving.
 */
function basetis_gdpr_form_validate($form, &$form_state) {
  $arg_3 = arg(3);
  $arg_4 = arg(4);

  // Validations on insert values.
  if (isset($arg_3) and ($arg_3 == 'add')) {
    $paths_names = basetis_gdpr_validation_names();

    // The path is saved without spaces at the beging or ending of the string.
    $form['path']["#value"] = trim($form['path']["#value"]);
    $new_path = $form['path']["#value"];
    foreach ($paths_names as $key) {
      $path_name = $key->path;
      if ($path_name == $new_path or $path_name . "/" == $new_path or $path_name == $new_path . "/") {
        form_set_error('ruta_val', t('The path already exists and must be unique'));
      }
    }
    if (strlen($form['path']["#value"]) > 255) {
      form_set_error('ruta_long', t('The path must exists and its length must be under 255 characters'));
    }
    if (!is_numeric($form['level']["#value"])) {
      form_set_error('level_num', t('The level must be numeric'));
    }
    if (strlen($form['level']["#value"]) > 10) {
      form_set_error('level_long', t('The level field must not contain more than 10 characteres'));
    }
    if (!isset($form['level']["#value"]) or ($form['level']["#value"] == '0')) {
      form_set_error('level_value', t('The level must have a value'));
    }
    if (isset($form['description']["#value"]) and (strlen($form['description']["#value"]) > 255)) {
      form_set_error('desc_long', t('The length of the description must be under 255 characteres'));
    }
  }

  // Validations for the values changed/introduced.
  else if (isset($arg_3) and isset($arg_4) and is_numeric($arg_3) and ($arg_4 == 'edit')) {

    //The path is saved without spaces at the beging or ending of the string.
    $form['path']["#value"] = trim($form['path']["#value"]);
    if ($form['path']["#value"] != $form['path']["#default_value"]) {
      $paths_names = basetis_gdpr_validation_names();
      foreach ($paths_names as $key) {
        $path_name = $key->path;
        if ($path_name == $form['path']["#value"]) {
          form_set_error('ruta_val', t('The path already exists and must be unique'));
        }
      }
      if (strlen($form['path']["#value"]) > 255) {
        form_set_error('ruta_long', t('The path must exists and its length must be under 255 characters'));
      }
    }
    if (!is_numeric($form['level']["#value"])) {
      form_set_error('level_num', t('The level must be numeric'));
    }
    if (strlen($form['level']["#value"]) > 10) {
      form_set_error('level_long', t('The level field must not contain more than 10 characteres'));
    }
    if (isset($form['description']["#value"]) and (strlen($form['description']["#value"]) > 255)) {
      form_set_error('desc_long', t('The length of the description must be under 255 characteres'));
    }
  }
}

/**
 * $SESSION variables are defined to keep track of the filter search parameters.
 *
 * Used in basetis_gdpr_get_table_params(), $form['filtre']['path'] and $form['filtre']['level'].
 */
function basetis_gdpr_submit_buscar_submit($form, &$form_state) {
  $_SESSION['basetis_gdpr_filtre']['path'] = $form['filtre']['path']["#value"];
  $_SESSION['basetis_gdpr_filtre']['level'] = $form['filtre']['level']["#value"];
}

/**
 * Deletes the rows checked using the function basetis_gdpr_delete.
 */
function basetis_gdpr_submit_borrar_submit($form, &$form_state) {
  $checked_ids = array();
  foreach ($form_state["values"]["tabla"] as $key) {
    if (isset($form_state["values"]["tabla"][$key]) and $form_state["values"]["tabla"][$key] == $key) {
      $id = $form_state["complete form"]["tabla"]["#options"][$key]["id"];
      $checked_ids[] = $id;
    }
  }
  if (basetis_gdpr_delete($form, $checked_ids) == "false") {
    drupal_set_message(t('Has been an error in the delete process'), 'error');
  }
}

/**
 * When pressing insert redirects to the insert form.
 */
function basetis_gdpr_submit_go_to_insert_submit($form, &$form_state) {
  drupal_goto('admin/gdpr/sensitive_pages/levels/add');
}

/**
 * After all necessary returns to the main page of the table.
 */
function basetis_gdpr_form_submit($form, &$form_state) {
  $arg_3 = arg(3);
  $arg_4 = arg(4);
  if (isset($arg_3) and ($arg_3 == 'add')) {
    if (basetis_gdpr_insert($form)) {
      drupal_goto('admin/gdpr/sensitive_pages');
    }
    else {
      drupal_set_message(t('Has been an error in the insertion process'), 'error');
    }
  }
  elseif (isset($arg_3) and isset($arg_4) and is_numeric($arg_3) and ($arg_4 == 'edit')) {
    if (basetis_gdpr_edit($form)) {
      drupal_goto('admin/gdpr/sensitive_pages');
    }
    else {
      drupal_set_message(t('Has been an error in the edit process'), 'error');
    }
  }
}

/**
 * Permissions are given.
 */
function basetis_gdpr_permission() {
  return array(
    'access basetis gdpr pages form' => array(
      'title' => t('access basetis gdpr pages form Module'),
      'description' => t('access basetis gdpr pages form Module'),
    ),
    'access basetis gdpr menu' => array(
      'title' => t('access basetis gdpr menu Module'),
      'description' => t('access basetis gdpr menu Module'),
    ),
  );
}
