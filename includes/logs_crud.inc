<?php

/**
 * Logs table with filter and cron.
 */

/**
 * A form to see the entrances to pages with sensitive levels.
 */
function basetis_gdpr_logs_form($form, &$form_state) {
  $form = basetis_gdpr_logs_list($form, $form_state);
  return $form;
}

/**
 * Creates the form shown when entering admin/gdpr/logs.
 */
function basetis_gdpr_logs_list($form, $form_state) {
  drupal_add_css('https://use.fontawesome.com/releases/v5.4.2/css/all.css', 'external');
  drupal_add_js(drupal_get_path('module', 'basetis_gdpr') . '/js/basetis_gdpr_cron.js');
  drupal_add_css(drupal_get_path('module', 'basetis_gdpr') . '/css/basetis_gdpr_cron.css');

  $form['text_1'] = array(
    '#markup' => '<p id="text_1"> ' . t("This page displays the data related with the access to sensitive pages: who has seen a certain sensitive page, if a pop-up with a message advising that the page had sensitive data was shown, if the pop-up was accepted and when this happened.") . '</p>',
  );

  // A filter is made with options to filter by user or page path.
  $user_id = basetis_gdpr_get_users_id_options();
  $form['filtre_log']['start_div'] = array(
    '#markup' => '<div class= "inicio_filtro">',
  );
  $form['filtre_log']['text_1'] = array(
    '#markup' => '<div  style="
    font-size: 0.923em;
    margin: 0;
    padding-bottom: 9px;
    font-weight: bold;">' . t("FILTER") . '</div>',
  );

  $form['filtre_log']['user_id'] = array(
    '#prefix' => '<div class="filtro_tabla">',
    '#type'          => 'select',
    '#title'         => t('User Id'),
    '#options' => $user_id,
    '#default_value' => isset($_SESSION['basetis_gdpr_filtre_log']['user_id']) ? $_SESSION['basetis_gdpr_filtre_log']['user_id'] : '',
    '#suffix' => '</div>',
  );

  $form['filtre_log']['page_path'] = array(
    '#prefix' => '<div class="filtro_tabla">',
    '#type'          => 'textfield',
    '#title'         => t('Path'),
    '#default_value' => isset($_SESSION['basetis_gdpr_filtre_log']['page_path']) ? $_SESSION['basetis_gdpr_filtre_log']['page_path'] : '',
    '#size'          => 15,
    '#maxlength'     => 15,
    '#suffix' => '</div>',
  );

  $form['filtre_log']['submit'] = array(
    '#prefix' => '<div class="filtro_tabla">',
    '#type'     => 'submit',
    '#value'    => t('Find'),
    '#submit'    => array('basetis_gdpr_submit_buscar_submit'),
    '#suffix' => '</div>',
  );

  $form['filtre_log']['end_div'] = array(
    '#markup' => '</div>',
  );

  $tableData = basetis_gdpr_get_table_data();
  $tableHeader = $tableData['tableHeader'];
  $tableRows = $tableData['tableRows'];

  $form['tabla'] = array(
    '#prefix' => '<div>',
    '#theme'  => 'table',
    '#header' => $tableHeader,
    '#rows'   => $tableRows,
    '#empty'  => t('No content available.'),
    '#sufix'  => '</div>',
  );

  $form['pager'] = array('#markup' => theme('pager'));

  $deletion_period = variable_get('basetis_gdpr_logs_cron_period');
  $options = array(
    '1' => t('1 month'),
    '3' => t('3 months'),
    '6' => t('6 months'),
    '12' => t('1 year'),
  );

  $form['button_show_modal'] = array(
    '#markup' => '<a class="button" class="report-item" onclick="javascript:modal()" > ' . t('Configure cron') . '<i class="fas fa-cog" style="margin-left: 10px;"></i></a>',
  );

  // Start modal.
  $form['cron_modal']['start_modal'] = array(
    '#markup' => '<div id="cron_modal" class="cron_modal" style="display: none"><div class="cron_modal-content">',
  );

  // Header.
  $form['cron_modal']['header'] = array(
    '#markup' => '<div class="cron_modal-header"></div>',
  );

  // Body.
  $form['cron_modal']['body'] = array(
    '#prefix' => '<div class="cron_modal-body">',
    '#type' => 'radios',
    '#title' => t('Select how long you want the registers to last:'),
    '#default_value' => $deletion_period,
    '#options' => $options,
    '#suffix' => '</div>',
  );

  $form['cron_modal']['footer_change'] = array(
    '#prefix' => '<div class="cron_modal-footer">',
    '#markup' => '<a class="button button-cancel" class="report-item" onclick="jQuery(\'#cron_modal\').hide()">' . t('Cancel') . '</a>',
  );

  $form['cron_modal']['footer_go_back'] = array(
    '#type'     => 'submit',
    '#value'    => t('Change'),
    '#attributes' => array(
      'class' => array('btn btn-danger pull-right'),
      'id' => 'button-change',
    ),
    '#suffix' => '</div>',
  );

  // End modal.
  $form['cron_modal']['end_modal'] = array(
    '#markup' => '</div></div>',
  );

  return $form;
}

/**
 * Prepairs the array to use as options in the filter of the form.
 * Used in $form['filtre_log']['user_id'].
 */
function basetis_gdpr_get_users_id_options() {
  $sol = basetis_gdpr_get_users_id();
  $user_id = array();
  $user_id['0'] = '';
  foreach ($sol as $rowdata) {
    $user_id[$rowdata->user_id] = $rowdata->name;
  }
  if (count($user_id) == 1) {
    $user_id['0'] = t('No content available.');
  }
  return $user_id;
}

/**
 * Selects the different users that entered a sensitive page.
 * Used in basetis_gdpr_get_users_id_options().
 */
function basetis_gdpr_get_users_id() {
  $query = db_select('basetis_gdpr_logs', 'l');
  $query->leftJoin('{users}', 'u', 'l.user_id = u.uid');
  $query->fields('l', array('user_id'));
  $query->fields('u', array('name'));
  $query->groupBy('user_id');
  try {
    $results = $query->execute();
  }
  catch (Exception $e) {
    return FALSE;
  }
  return $results;
}

/**
 * Updates params.
 */
function basetis_gdpr_get_table_params() {
  $params = array(
    'page_path' => isset($_SESSION['basetis_gdpr_filtre_log']['page_path']) ? $_SESSION['basetis_gdpr_filtre_log']['page_path'] : NULL,
    'user_id'  => isset($_SESSION['basetis_gdpr_filtre_log']['user_id']) ? $_SESSION['basetis_gdpr_filtre_log']['user_id'] : NULL,
  );

  return $params;
}

/**
 * The structure of the table is created.
 * The data of the table is used in $form['tabla'].
 */
function basetis_gdpr_get_table_data() {

  $tableHeader = array(
    'id'          => array('data' => 'ID', 'field' => 'id', 'sort' => 'DESC'),
    'user_id'     => array('data' => t('User ID'), 'field' => 'user_id'),
    'page_id'     => array('data' => t('Page ID'), 'field' => 'page_id'),
    'path'        => array('data' => t('Path'), 'field' => 'path'),
    'msg_shown'   => array('data' => t('Message shown'), 'field' => 'msg_shown'),
    'msg_accepted'   => array('data' => t('Message accepted'), 'field' => 'msg_accepted'),
    'log_date'       => array('data' => t('Date'), 'field' => 'log_date', 'sort' => 'DESC'),
  );

  $params = basetis_gdpr_get_table_params();
  $data = basetis_gdpr_get_table_records($tableHeader, $params);
  $tableRows = array();
  foreach ($data as $rowdata) {
    $tableRows[$rowdata->id] = array(
      'id'           => $rowdata->id,
      'user_id'      => $rowdata->name,
      'page_id'      => $rowdata->page_id,
      'path'         => check_plain($rowdata->path),
      'msg_shown'    => $rowdata->msg_shown,
      'msg_accepted' => $rowdata->msg_accepted,
      'log_date'     => $rowdata->log_date,
    );
  }

  $returnTable = array(
    'tableHeader' => $tableHeader,
    'tableRows' => $tableRows,
  );

  return $returnTable;
}

/**
 * Data to show in the table is taken from the table basetis_gdpr_logs.
 */
function basetis_gdpr_get_table_records($tableHeader, $params) {
  $query = db_select('basetis_gdpr_logs', 'l')
    ->extend('PagerDefault')
    ->limit(15)
    ->extend('TableSort')
    ->orderByHeader($tableHeader);
  $query->leftJoin('users', 'u', 'l.user_id = u.uid');
  $array_fields = array(
    'id',
    'page_id',
    'path',
    'msg_shown',
    'msg_accepted',
    'log_date'
  );
  $query->fields('l', $array_fields);
  $query->fields('u', array('name'));
  if (isset($params['page_path']) and $params['page_path'] != '') {
    $query->condition('path', $params['page_path'] . '%','LIKE');
  }
  if (isset($params['user_id']) and $params['user_id'] != '0') {
    $query->condition('user_id', $params['user_id'], '=');
  }
  $query->orderBy('log_date', 'DESC');
  try {
    $results = $query->execute();
  }
  catch (Exception $e) {
    return FALSE;
  }
  return $results;
}

/**
 * Updates the value of the variable basetis_gdpr_logs_cron_period.
 */
function basetis_gdpr_change_cron_period_value($form) {
  $deletion_period = $form['cron_modal']['body']['#value'];
  variable_set('basetis_gdpr_logs_cron_period', $deletion_period);
}

/**
 * $SESSION variables defined to keep track of the filter search parameters.
 * Used in basetis_gdpr_get_table_params(),
 * $form['filtre_log']['page_path'] and $form['filtre_log']['user_id'].
 */
function basetis_gdpr_submit_buscar_submit($form, &$form_state) {
  $_SESSION['basetis_gdpr_filtre_log']['page_path'] = $form['filtre_log']['page_path']["#value"];
  $_SESSION['basetis_gdpr_filtre_log']['user_id'] = $form['filtre_log']['user_id']["#value"];
}

/**
 * Validations nedeed before submit.
 */
function basetis_gdpr_logs_form_validate($form, &$form_state) {
}

/**
 * When submit return $form.
 */
function basetis_gdpr_logs_form_submit($form, &$form_state) {
  basetis_gdpr_change_cron_period_value($form);
}

/**
 * Permissons are given.
 */
function basetis_gdpr_logs_permission() {
  return array(
    'access basetis gdpr logs form' => array(
      'title' => t('access basetis gdpr logs form Module'),
      'description' => t('access basetis gdpr logs form Module'),
    ),
    'access basetis gdpr menu' => array(
      'title' => t('access basetis gdpr menu Module'),
      'description' => t('access basetis gdpr menu Module'),
    ),
  );
}
