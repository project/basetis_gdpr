<?php

/**
 * Sensitive levels crud.
 */

/**
 * Depending on the part of the page selected a different form will be used.
 */
function basetis_gdpr_levels_form($form, &$form_state) {
  $arg_4 = arg(4);
  $arg_5 = arg(5);
  if (isset($arg_4) and ($arg_4 == 'add')) {
    $form = basetis_gdpr_levels_insert_list($form, $form_state);
  }
  elseif (isset($arg_4) and is_numeric($arg_4) and isset($arg_5) and ($arg_5 == 'edit')) {
    $form = basetis_gdpr_levels_edit_list($form, $form_state);
  }
  else {
    $form = basetis_gdpr_levels_list($form, $form_state);
  }
  return $form;
}

/**
 * Prepairs an array with the role names.
 *
 * Used in $form['filtre']['rol'] and $_SESSION['basetis_gdpr_used_rols'][$rid].
 */
function basetis_gdpr_list_rols() {
  $rols = basetis_gdpr_get_table_rols();
  $nom_rols = array();
  $nom_rols[] = '';
  foreach ($rols as $key) {
    $nom_rols[$key->rid] = $key->name;
  }
  return $nom_rols;
}

/**
 * Prepairs an array with available roles.
 */
function basetis_gdpr_available_rols() {
  $rols = basetis_gdpr_get_table_rols();
  $av_rols = array();
  $av_rols[] = '';
  foreach ($rols as $rol) {
    $used = '0';
    foreach ($_SESSION['basetis_gdpr_used_rols'] as $used_rol) {
      if ($rol->name == $used_rol['name']) {
        $used = '1';
      }
    }
    if ($used == '0') {
      $av_rols[$rol->rid] = $rol->name;
    }
  }
  return $av_rols;
}

/**
 * It shows the table of sensitive pages.
 * Lets you edit, insert, delete and filter data.
 */
function basetis_gdpr_levels_list($form, $form_state) {
  drupal_add_js(drupal_get_path('module', 'basetis_gdpr') . '/js/basetis_gdpr.js');
  drupal_add_css('https://use.fontawesome.com/releases/v5.4.2/css/all.css', 'external');
  drupal_add_css(drupal_get_path('module', 'basetis_gdpr') . '/css/basetis_gdpr_delete.css');

  $form['insert']['start_div'] = array(
    '#markup' => '<div>',
  );
  $url_to_go = url('admin/gdpr/sensitive_pages/levels/add');
  $form['insert']['insert_button'] = array(
    '#markup' => '<a class="button" class="block_elem" href="' . $url_to_go . '" style="padding-right: 10px;">' . t('Insert data') . '<i class="fa fa-plus" style="margin-left: 10px;"></i> </a>',
  );
  $form['insert']['text_2'] = array(
    '#markup' => '<div class="block_elem"><p id="text_2">' . t("This table contains important data of the different sensitive levels.") . '
    <br> ' . t("You can insert new levels, edit levels that already exist or delete some of them.") . '
    </p></div>',
  );

  $form['insert']['end_div'] = array(
    '#markup' => '</div>',
  );
  // It empties the global variables.
  // $_SESSION['basetis_gdpr_used_rols']->field_reviewers = array();
  $_SESSION['basetis_gdpr_default_level_data'] = array();

  // Variables thar are going to be use are created and defined.
  // Global variables that are used in many places of the file and also
  // $_SESSION['basetis_gdpr_used_rols'] is usde in basetis_gdpr.module.
  $_SESSION['basetis_gdpr_used_rols'] = array();
  $_SESSION['basetis_gdpr_first_charge'] = TRUE;
  $_SESSION['basetis_gdpr_last_id'] = '-1';
  $tableData = basetis_gdpr_get_levels_table_data();
  $tableHeader = $tableData['tableHeader'];
  $tableRows = $tableData['tableRows'];
  $nom_rols = basetis_gdpr_list_rols();
  $form['filtre']['start_div'] = array(
    '#markup' => '<div class= "inicio_filtro">',
  );
  $form['filtre']['text_1'] = array(
    '#markup' => '<div  style="
    font-size: 0.923em;
    margin: 0;
    padding-bottom: 9px;
    font-weight: bold;">' . t("FILTER") . '</div>',
  );
  $form['filtre']['nom_level'] = array(
    '#prefix' => '<div class="filtro_tabla">',
    '#type'          => 'textfield',
    '#title'         => t('Level name'),
    '#default_value' => isset($_SESSION['basetis_gdpr_filtre']['nom_level']) ? $_SESSION['basetis_gdpr_filtre']['nom_level'] : '',
    '#size'          => 15,
    '#maxlength'     => 15,
    '#suffix' => '</div>',
  );

  $form['filtre']['rol'] = array(
    '#prefix' => '<div class="filtro_tabla">',
    '#type'          => 'select',
    '#title'         => t('Rol'),
    '#options' => $nom_rols,
    '#default_value' => isset($_SESSION['basetis_gdpr_filtre']['rol']) ? $_SESSION['basetis_gdpr_filtre']['rol'] : '',
    '#suffix' => '</div>',
  );

  $form['filtre']['submit'] = array(
    '#prefix' => '<div class="filtro_tabla">',
    '#type'     => 'submit',
    '#value'    => t('Find'),
    '#submit'    => array('basetis_gdpr_submit_buscar_submit'),
    '#suffix' => '</div>',
  );

  $form['filtre']['end_div'] = array(
    '#markup' => '</div>',
  );

  // Text where you can introduce the name of the level.
  // Selector to select a certain rol.
  $form['tabla'] = array(
    '#prefix' => '<div>',
    '#type' => 'tableselect',
    '#header' => $tableHeader,
    '#options' => $tableRows,
    '#empty' => t('No content available.'),
    '#multiple' => TRUE,
    '#sufix' => '</div>',
  );
  $form['pager'] = array('#markup' => theme('pager'));

  $form['button_delete'] = array(
    '#markup' => '<a class="button" class="report-item" onclick="javascript:update_text()"> ' . t('Delete selected records') . '<i class="far fa-trash-alt" style="margin-left: 10px;"></i></a>',
  );

  $modal_messages = array(
    'noItem' => t('There is no item to delete'),
    'itemText1' => t('Are you sure?'),
    'itemText2' => t('The next items are going to be deleted:'),
    'and' => t('and'),
  );

  drupal_add_js(array('basetis_gdpr' => array('modal_messages' => $modal_messages)), 'setting');

  $form['delete_rows'] = array(
    '#prefix' => '<div id="delete_modal" class="delete_modal" style="display: none">
                    <!-- Modal content -->
                    <div class="delete_modal-content">
                      <div class="delete_modal-header">
                      </div>
                      <div class="delete_modal-body">
                        <h2><i class="fa fa-exclamation-triangle fa-2x" style="color: #d45867;"></i></h2>
                        <p id="text_ad" style="font-weight: bold;">Are you sure?</p>
                        <p id="checkbox_values"></p>
                      </div>
                      <div class="delete_modal-footer">
                        <a class="button button-cancel" class="report-item" onclick="javascript:modal()">' . t('Cancel') . '</a>',
    '#type'   => 'submit',
    '#value'  => t('Delete'),
    '#submit' => array('basetis_gdpr_submit_borrar_submit'),
    '#attributes' => array(
      'class' => array('btn btn-danger pull-right'),
      'id' => 'button-accept-delete',
    ),
    '#sufix'  => '    </div>
                    </div>
                  </div>',
  );
  return $form;
}

/**
 * Creates the form shown when entering admin/gdpr/levels/add.
 */
function basetis_gdpr_levels_insert_list($form, $form_state) {
  drupal_add_css('https://use.fontawesome.com/releases/v5.4.2/css/all.css', 'external');
  drupal_add_css(drupal_get_path('module', 'basetis_gdpr') . '/css/basetis_gdpr_delete.css');
  $link_select = url('admin/gdpr/sensitive_pages/levels');

  $form['back_to_table'] = array(
    '#markup' => '<div><a href="' . $link_select . '" class="form-submit"><i class="fa fa-arrow-circle-left"></i> ' . t('Go back') . '</a></div>',
  );

  $form['text_3'] = array(
    '#markup' => '<p id="text_3"> ' . t("This page offers the possibility to add a sensitive level with its characteristics.") . '</p>',
  );
  $form['start_first_row_div'] = array(
    '#markup' => '<div class="row_2_elem">',
  );
  $form['level'] = array(
    '#prefix' => '<div class = "block_elem default_spaced" >',
    '#type' => 'textfield',
    '#title' => t('Level'),
    '#default_value' => isset($_SESSION['basetis_gdpr_default_level_data']['level']) ? $_SESSION['basetis_gdpr_default_level_data']['level'] : '',
    '#description' => t('Add the level name'),
    '#required' => TRUE,
    '#size' => 35,
    '#suffix' => '</div>',
  );

  $form['description'] = array(
    '#prefix' => '<div class = "block_elem default_spaced">',
    '#type' => 'textfield',
    '#title' => t('Description'),
    '#default_value' => isset($_SESSION['basetis_gdpr_default_level_data']['description']) ? $_SESSION['basetis_gdpr_default_level_data']['description'] : '',
    '#description' => t('Add the level description'),
    '#suffix' => '</div>',
  );
  $form['end_first_row_div'] = array(
    '#markup' => '</div>',
  );
  $form['start_second_row_div'] = array(
    '#markup' => '<div class="row_2_elem">',
  );

  $form['frequence'] = array(
    '#prefix' => '<div class = "block_elem default_spaced">',
    '#type' => 'textfield',
    '#title' => t('Default frequence'),
    '#default_value' => isset($_SESSION['basetis_gdpr_default_level_data']['frequence']) ? $_SESSION['basetis_gdpr_default_level_data']['frequence'] : '',
    '#description' => t('Add the message default frequence. Below you can personalize the frequence depending on the role. The frequence will be set in days.'),
    '#size' => 35,
    '#suffix' => '</div>',
  );

  $form['end_second_row_div'] = array(
    '#markup' => '</div>',
  );

  $form['new_role_text']['text'] = array(
    '#markup' => '<p class="inicio_filtro" id="new_role_text">' . t("Even if you add customized data for a certain role, if the page where this level is used does not have acces for that role the message will not be shown.") . '</p>',
  );

  if ($_SESSION['basetis_gdpr_last_id'] != '-1') {
    $_SESSION['basetis_gdpr_first_charge'] = TRUE;
    $_SESSION['basetis_gdpr_used_rols'] = array();
    $_SESSION['basetis_gdpr_last_id'] = '-1';
  }

  if (isset($_SESSION['basetis_gdpr_used_rols']) && (count($_SESSION['basetis_gdpr_used_rols']) > 0)) {
    $form['fixed'][start_div] = array(
      '#markup' => '<div class= "row_2_elem">',
    );
    $form['fixed']['column_1'] = array(
      '#markup' => '<div class = "filtro_tabla rol_level_elem" style="font-weight: bold;"">' . t('Access rol') . '</div>',
    );
    $form['fixed']['column_2'] = array(
      '#markup' => '<div class = "filtro_tabla rol_level_elem" style="font-weight: bold;"">' . t('Frequence') . '</div>',
    );
    $form['fixed'][end_div] = array(
      '#markup' => '</div>',
    );
    foreach ($_SESSION['basetis_gdpr_used_rols'] as $rol) {
      $rid = '';
      if (isset($rol['rid'])) {
        $rid = $rol['rid'];
      }
      $form['fixed'][$rid]['start_row'] = array(
        '#markup' => '<div class="row_2_elem">',
      );
      $form['fixed'][$rid]['roles'] = array(
        '#prefix' => '<div class="block_elem rol_level_elem">',
        '#type' => 'item',
        '#default_value' => $rol["name"],
        '#markup' => $rol["name"],
        '#size' => 35,
        '#suffix' => '</div>',
      );
      $form['fixed'][$rid]['frequence_per'] = array(
        '#prefix' => '<div class="block_elem rol_level_elem">',
        '#type'     => 'item',
        '#default_value' => $rol["frequence"],
        '#markup' => $rol["frequence"],
        '#size' => 35,
        '#suffix' => '</div>',
      );

      $form['fixed'][$rid]['delete'] = array(
        '#prefix' => '<div class="filtro_tabla rol_level_elem">',
        '#type'   => 'submit',
        '#value'  => t('Delete'),
        '#name'  => 'delete_' . $rid,
        '#submit' => array('submit_insert_delete_submit'),
        '#suffix' => '</div>',
      );
      $form['fixed'][$rid]['end_row'] = array(
        '#markup' => '</div>',
      );
    }
    $form['fixed']['end_div'] = array(
      '#markup' => '</div>',
    );
  }

  $form['new_role']['start_div'] = array(
    '#markup' => '<div class="row_2_elem">',
  );

  $nom_rols = basetis_gdpr_available_rols();
  $form['new_role']['access_rols_add'] = array(
    '#prefix' => '<div class="filtro_tabla rol_level_elem ">',
    '#type'          => 'select',
    '#title'         => t('Access rols'),
    '#options'       => $nom_rols,
    '#suffix' => '</div>',
  );

  $form['new_role']['frequence_add'] = array(
    '#prefix' => '<div class="filtro_tabla rol_level_elem">',
    '#type' => 'textfield',
    '#title' => t('Frequence'),
    '#size' => 20,
    '#suffix' => '</div>',
  );

  $form['new_role']['add'] = array(
    '#prefix' => '<div class="filtro_tabla rol_level_elem"  style="margin-left: 40px;">',
    '#type' => 'submit',
    '#value' => t('Add'),
    '#submit' => array('submit_personalized_submit'),
    '#suffix' => '</div>',
  );

  $form['new_role']['end_div'] = array(
    '#markup' => '</div>',
  );

  $form['add_text_content'] = array(
    '#prefix' => '<div >',
    '#type' => 'submit',
    '#value'  => t('Add text message and save'),
    '#submit' => array('basetis_gdpr_submit_add_text_content_submit'),
    '#sufix' => '</div>',
  );
  return $form;
}

/**
 * Form shown when entering admin/gdpr/sensitive_pages/levels/id/edit
 */
function basetis_gdpr_levels_edit_list($form, $form_state) {
  drupal_add_css('https://use.fontawesome.com/releases/v5.4.2/css/all.css', 'external');
  drupal_add_css(drupal_get_path('module', 'basetis_gdpr') . '/css/basetis_gdpr_delete.css');

  $link_select = url('admin/gdpr/sensitive_pages/levels');

  $form['back_to_table'] = array(
    '#markup' => '<div><a href="' . $link_select . '" class="form-submit"><i class="fa fa-arrow-circle-left"></i> ' . t('Go back') . '</a></div>',
  );

  $form['text_4'] = array(
    '#markup' => '<p id="text_4"> ' . t("This page offers the possibility to edit the selected row of the table: modify the level name or the parameters related to it.") . '</p>',
  );

  $argument = arg(4);
  $params = array(
    'edit_id'  => isset($argument) ? $argument : NULL,
  );
  $recieved_levels = basetis_gdpr_get_levels_table_records($form, $params);
  foreach ($recieved_levels as $rowdata) {
    $level = $rowdata->level;
    $description = $rowdata->description;
    $frequence = $rowdata->frequence;
  }

  $form['start_first_row_div'] = array(
    '#markup' => '<div class="row_2_elem">',
  );

  $form['level'] = array(
    '#prefix' => '<div class = "block_elem default_spaced">',
    '#type' => 'textfield',
    '#title' => t('Level name'),
    '#default_value' => $level,
    '#description' => t('If you modify the level name remember there should not exist another level with this name.'),
    '#required' => TRUE,
    '#size' => 35,
    '#suffix' => '</div>',
  );

  $form['description'] = array(
    '#prefix' => '<div class = "block_elem default_spaced">',
    '#type' => 'textfield',
    '#title' => t('Description'),
    '#default_value' => $description,
    '#description' => t('This page offers the possibility to change the level description or delte it, otherwise it will stay the same.'),
    '#suffix' => '</div>',
  );
  $form['end_first_row_div'] = array(
    '#markup' => '</div>',
  );

  $form['start_second_row_div'] = array(
    '#markup' => '<div class="row_2_elem">',
  );
  $form['frequence'] = array(
    '#prefix' => '<div class = "block_elem default_spaced">',
    '#type' => 'textfield',
    '#title' => t('Default frequence'),
    '#default_value' => $frequence,
    '#required' => TRUE,
    '#size' => 35,
    '#description' => t('This page offers the possibility to change the level default frequence, otherwise it will stay the same. The frequence will be set in days.'),
    '#suffix' => '</div>',
  );

  $form['end_second_row_div'] = array(
    '#markup' => '</div>',
  );

  // Looks if the id is the last one said to edit.
  // If not resets the global variables needed.
  if ($_SESSION['basetis_gdpr_last_id'] != $argument) {
    $_SESSION['basetis_gdpr_first_charge'] = TRUE;
    $_SESSION['basetis_gdpr_used_rols'] = array();
  }
  $_SESSION['basetis_gdpr_last_id'] = $argument;

  if ($_SESSION['basetis_gdpr_first_charge'] == TRUE) {
    $personalized_data = basetis_gdpr_get_edit_data();
  }

  // You can introduce new personalized registers.
  $form['new_role_text']['text'] = array(
    '#markup' => '<p class= "inicio_filtro" id="new_role_text">' . t("Even if you add customized data for a certain role, if the page where this level is used does not have acces for that role the message will not be shown.") . '</p>',
  );

  $nom_rols = basetis_gdpr_list_rols();
  $error_message = '';
  if ($_SESSION['basetis_gdpr_first_charge'] == TRUE) {
    foreach ($personalized_data as $row) {
      $error_message = '';
      $rid = $row->rid;
      if (!isset($nom_rols[$rid])) {
        $name = $row->role_name;
        $error_message = t('The role has been deleted form the table role. We recomend you to delete this register and make a new one.');
      }
      else {
        $name = $nom_rols[$rid];
      }
      $frequence = $row->frequence;
      $_SESSION['basetis_gdpr_used_rols'][$rid] = array(
        'rid'     => $rid,
        'name'    => $name,
        'frequence'     => $frequence,
        'error_message' => $error_message,
      );
      $_SESSION['basetis_gdpr_first_charge'] = FALSE;
    }
  }

  // Shows the personalized registers at the moment.
  if (isset($_SESSION['basetis_gdpr_used_rols']) and (count($_SESSION['basetis_gdpr_used_rols']) > 0)) {

    $form['pers_edit']['start_div'] = array(
      '#markup' => '<div class="row_2_elem">',
    );
    $form['pers_edit']['column_1'] = array(
      '#markup' => '<div class = "block_elem rol_level_elem" style="font-weight: bold;">' . t('Access rol') . '</div>',
    );
    $form['pers_edit']['column_2'] = array(
      '#markup' => '<div class = "block_elem rol_level_elem" style="font-weight: bold;">' . t('Frequence') . '</div>',
    );

    foreach ($_SESSION['basetis_gdpr_used_rols'] as $rol) {
      $rid = $rol['rid'];

      $form['pers_edit'][$rid]['start_row'] = array(
        '#markup' => '<div class="row_2_elem">',
      );

      $form['pers_edit'][$rid]['roles_edit'] = array(
        '#prefix' => '<div class="block_elem rol_level_elem">',
        '#type' => 'item',
        '#default_value' => $rol["name"],
        '#markup' => $rol["name"],
        '#suffix' => '</div>',
      );
      $form['pers_edit'][$rid]['frequence_edit'] = array(
        '#prefix' => '<div class="block_elem rol_level_elem">',
        '#type'     => 'item',
        '#default_value' => $rol["frequence"],
        '#markup' => $rol["frequence"],
        '#suffix' => '</div>',
      );

      $form['pers_edit'][$rid]['pers_edit'] = array(
        '#prefix' => '<div class="filtro_tabla block_elem">',
        '#type'   => 'submit',
        '#value'  => t('Delete'),
        '#name'  => 'delete_' . $rid,
        '#submit' => array('submit_insert_delete_submit'),
        '#suffix' => '</div>',
      );

      if ($rol["error_message"] != '') {
        $form['pers_edit'][$rid]['error_message'] = array(
          '#markup' => '<p class="block_elem" id="error_message"><i class="fas fa-exclamation-triangle" style="color: red;"></i>' . $rol["error_message"] . '</p>',
        );
      }

      $form['pers_edit'][$rid]['end_row'] = array(
        '#markup' => '</div>',
      );
    }
    $form['pers_edit']['end_div'] = array(
      '#markup' => '</div>',
    );
  }

  $form['new_role']['start_div'] = array(
    '#markup' => '<div class="row_2_elem">',
  );
  $nom_rols = basetis_gdpr_available_rols();

  $form['new_role']['access_rols_add'] = array(
    '#prefix' => '<div class="filtro_tabla rol_level_elem ">',
    '#type' => 'select',
    '#title' => t('Access rols'),
    '#options' => $nom_rols,
    '#suffix' => '</div>',
  );

  $form['new_role']['frequence_add'] = array(
    '#prefix' => '<div class="filtro_tabla rol_level_elem">',
    '#type' => 'textfield',
    '#title' => t('Frequence'),
    '#size' => 20,
    '#suffix' => '</div>',
  );

  $form['new_role']['add'] = array(
    '#prefix' => '<div class="filtro_tabla rol_level_elem"  style="margin-left: 40px;">',
    '#type' => 'submit',
    '#value'  => t('Add'),
    '#submit' => array('submit_personalized_submit'),
    '#suffix' => '</div>',
  );

  $form['new_role']['end_div'] = array(
    '#markup' => '</div>',
  );

  // The function deletes all data existing to the level and fills it again.
  $form['edit'] = array(
    '#prefix' => '<div>',
    '#type' => 'submit',
    '#value' => t('Save'),
    '#sufix' => '</div>',
  );
  return $form;
}

function basetis_gdpr_get_levels_table_params() {
  $params = array(
    'filtre_nom_level'  => isset($_SESSION['basetis_gdpr_filtre']['nom_level']) ? $_SESSION['basetis_gdpr_filtre']['nom_level'] : NULL,
    'filtre_rol'    => isset($_SESSION['basetis_gdpr_filtre']['rol']) ? $_SESSION['basetis_gdpr_filtre']['rol'] : NULL,
  );
  return $params;
}

/**
 * The structure of the table is created.
 */
function basetis_gdpr_get_levels_table_data() {

  $tableHeader = array(
    'id',
    'level',
    'description',
    'text_message_nid',
    'frequence',
    'number_pages',
    'access_rols',
    'creation_date',
    'update_date',
    'edit'
  );
  $tableHeader2 = array(
    'level_name' => array('data' => t('Level name'), 'field' => 'level'),
    'description' => array('data' => t('Description'), 'field' => 'description'),
    'title_text_message' => array('data' => t('Title message'), 'field' => 'text_message'),
    'frequence' => array('data' => t('Text message frequence'), 'field' => 'frequence'),
    'number_pages' => array('data' => t('Number of pages using the level'), 'field' => 'number_pages'),
    'access_rols' => array('data' => t('Customized data'), 'field' => 'access_rols'),
    'creation_date' => array('data' => t('Creation date'), 'field' => 'session_date'),
    'update_date' => array('data' => t('Update date'), 'field' => 'update_date'),
    'translations' => array('data' => t('Text message translations')),
    'edit_text_message' => array('data' => t('Edit and translate text message')),
    'edit' => array('data' => t('Edit register')),
  );

  $params = basetis_gdpr_get_levels_table_params();
  $data = basetis_gdpr_get_levels_table_records($tableHeader, $params);

  $tableRows = array();
  foreach ($data as $rowdata) {
    $edit = '<a href="' . url('admin/gdpr/sensitive_pages/levels/' . $rowdata->id) . '/edit"><i class="fa fa-edit"></i></a>';
    $translate_path = 'node/' . $rowdata->text_message_nid . '/translate';
    if (!drupal_valid_path($translate_path)) {
      $edit_translate_text_message = '<a href="' . url('node/' . $rowdata->text_message_nid) . '/edit"><i class="fas fa-language fa-2x"></i></a>';
    } else {
      $edit_translate_text_message = '<a href="' . url('node/' . $rowdata->text_message_nid) . '/translate"><i class="fas fa-language fa-2x"></i></a>';
    }
    $av_languages = basetis_gdpr_available_languages($rowdata->text_message_nid);
    unset($available_translations);
    $available_translations = '';
    foreach ($av_languages as $key) {
      $available_translations = $key->name . '<br>' . $available_translations;
    }
    if ($available_translations == '' or COUNT($available_translations) == 0) {
      $available_translations = t('There is no translation available');
    }
    $sensitive_level = check_plain($rowdata->level);
    $description_level = check_plain($rowdata->description);
    $tableRows[$rowdata->id] = array(
      'id'            => $rowdata->id,
      'level_name'    => '<data value="' . $sensitive_level . '" id="label_' . $rowdata->id . '">' . $sensitive_level . '</data>',
      'level'         => $sensitive_level,
      'description'   => $description_level,
      'frequence'     => $rowdata->frequence,
      'title_text_message'  => $rowdata->title,
      'creation_date' => $rowdata->creation_date,
      'update_date'   => $rowdata->update_date,
      'number_pages'  => $rowdata->number_pages ? $rowdata->number_pages : 0,
      'access_rols'   => $rowdata->personalized_data,
      'translations'  => $available_translations,
      'edit_text_message'   => $edit_translate_text_message,
      'edit'          => $edit,
    );
  }

  $returnTable = array(
    'tableHeader' => $tableHeader2,
    'tableRows' => $tableRows,
  );

  return $returnTable;
}


/**
 * Languages that have translations for the content.
 */
function basetis_gdpr_available_languages($tnid) {
  $query = db_select('node', 'n');
  $query->leftJoin('languages', 'l', 'n.language = l.language');
  $query->fields('l', array('name'));
  $query->groupBy('n.language');
  $db_or = db_or();
  $db_or->condition('n.tnid', $tnid, '=');
  $db_or->condition('n.nid', $tnid, '=');
  $query->condition($db_or);

  try {
    $results = $query->execute();
  }
  catch (Exception $e) {
    return FALSE;
  }
  return $results;
}

/**
 * Gets the data for the table shown in sensitive levels.
 */
function basetis_gdpr_get_levels_table_records($tableHeader, $params) {
  $subquery = db_select('basetis_gdpr_sensitive_pages', 's');
  $subquery->groupBy('s.level');
  $subquery->addExpression('COUNT(s.level)', 'number_pages');
  $subarray_fields = array('level');
  $subquery->fields('s', $subarray_fields);
  $query = db_select('basetis_gdpr_page_levels', 'p')
    ->extend('PagerDefault')
    ->limit(4)
    ->extend('TableSort')
    ->orderByHeader($tableHeader);
  $query->leftJoin('basetis_gdpr_roles_levels', 'rl', 'p.id = rl.lid');
  $query->leftJoin('{node}', 'n', 'p.text_message_nid = n.nid');
  $query->leftJoin('role', 'r', 'rl.rid = r.rid');
  $query->addJoin('left', $subquery, 's', 'p.id = s.level');
  $array_fields = array('id',
    'level',
    'description',
    'frequence',
    'text_message_nid',
    'creation_date',
    'update_date'
  );
  $query->fields('p', $array_fields);
  $query->fields('n', array('title'));
  $query->addExpression("case when r.rid is not null then 'yes' else 'No' end", 'personalized_data');
  $query->fields('s', array('number_pages'));
  $query->condition('p.is_default', '0', '=');
  if (isset($params['filtre_nom_level']) and $params['filtre_nom_level'] != '') {
    $query->condition('p.level', $params['filtre_nom_level'] . '%', 'LIKE');
  }

  if (isset($params['filtre_rol']) and $params['filtre_rol'] != '0') {
    $query->condition('rl.rid', $params['filtre_rol'], '=');
  }
  if (isset($params['edit_id']) and is_numeric($params['edit_id'])) {
    $query->condition('p.id', $params['edit_id'], '=');
  }

  $query->groupBy('p.id');

  try {
    $results = $query->execute();
  }
  catch (Exception $e) {
    return FALSE;
  }
  return $results;
}

/**
 * Returns the roles ids and names.
 */
function basetis_gdpr_get_table_rols() {
  $query = db_select('role', 'p')
    ->extend('PagerDefault')
    ->extend('TableSort');
  $query->fields('p', array('rid', 'name'));
  $query->orderBy('rid');
  try {
    $results = $query->execute();
  }
  catch (Exception $e) {
    return FALSE;
  }
  return $results;
}

/**
 * Returns the values corresponding to the id to edit.
 */
function basetis_gdpr_get_edit_data() {
  $id = arg(4);
  $query = db_select('basetis_gdpr_roles_levels', 'rl')
    ->extend('PagerDefault')
    ->extend('TableSort');
  $query->fields('rl', array('rid', 'frequence', 'role_name'));
  $query->condition('rl.lid', $id, '=');

  try {
    $results = $query->execute();
  }
  catch (Exception $e) {
    return FALSE;
  }
  return $results;
}

/**
 * Saves the edited data.
 */
function basetis_gdpr_edit($form) {
  $level = $form['level']['#value'];
  $level = trim($level);
  $description = $form['description']['#value'];
  $frequence = $form['frequence']['#value'];
  $update_date = date('Y-m-d H:i:s');
  $id = arg(4);
  try {
    db_update('{basetis_gdpr_page_levels}')
      ->fields(array(
        'level'         => $level,
        'description'   => $description,
        'frequence'     => $frequence,
        'update_date'   => $update_date,
      ))
      ->condition('id',$id, '=')
      ->execute();

    $queryrl = db_delete('{basetis_gdpr_roles_levels}')
      ->condition('lid', $id, '=')
      ->execute();

    foreach ($_SESSION['basetis_gdpr_used_rols'] as $rol) {
      $rid = $rol['rid'];
      $frequence = $rol["frequence"];
      $role_name = $rol['name'];
      $queryrl = db_insert('{basetis_gdpr_roles_levels}')
        ->fields(array(
          'lid'   => $id,
          'rid'   => $rid,
          'frequence'     => $frequence,
          'role_name'     => $role_name,
        ));
      $queryrl->execute();
    }

  }
  catch (Exception $e) {
    return FALSE;
  }
  return TRUE;
}

/**
 * Deletes the rows with lid checked in the roles_levels.
 *
 * Deletes the rows with the id check in page_levels.
 */
function basetis_gdpr_delete($form, $ids) {
  $transaction = db_transaction();
  try {
    db_delete('{basetis_gdpr_roles_levels}')
      ->condition('lid', $ids, 'IN')
      ->execute();
    db_delete('{basetis_gdpr_page_levels}')
      ->condition('id', $ids, 'IN')
      ->condition('is_default', '0', '=')
      ->execute();
  }
  catch (Exception $e) {
    $transaction->rollback();
    return FALSE;
  }
}

/**
 * Selects de nid of the content text_message to be deleted.
 */
function basetis_gdpr_select_text_message_nid($ids) {
  $query = db_select('basetis_gdpr_page_levels', 'l');
  $query->condition('l.id', $ids, 'IN');
  $query->leftJoin('node', 'n', 'l.text_message_nid = n.tnid');
  $query->fields('n', array('nid'));
  try {
    $results = $query->execute();
  }
  catch (Exception $e) {
    return FALSE;
  }
  return $results;
}

/**
 * Returns the levels checked that are being used in the sensitive_pages.
 */
function basetis_gdpr_validation_delete($form, $checked_ids) {
  $query = db_select('basetis_gdpr_sensitive_pages', 'p')
    ->extend('PagerDefault')
    ->extend('TableSort');
  $query->leftJoin('basetis_gdpr_page_levels', 'l', 'p.level = l.id');
  $query->groupBy('p.level');
  $query->fields('p', array('level'));
  $query->fields('l', array('level'));
  $query->condition('p.level', $checked_ids, 'IN');
  try {
    $results = $query->execute();
  }
  catch (Exception $e) {
    return FALSE;
  }
  return $results;
}

/**
 * Gets levels names.
 */
function basetis_gdpr_validation_names() {
  $query = db_select('basetis_gdpr_page_levels', 'l')
    ->fields('l', array('level'));
  try {
    $results = $query->execute();
  }
  catch (Exception $e) {
    return FALSE;
  }
  return $results;
}

/**
 * General validations before saving data.
 */
function basetis_gdpr_levels_form_validate($form, &$form_state) {
  $triggering_element = $form_state['triggering_element']["#id"];

  // Validations are made depending on the submit button pressed.
  // For the general insert or edit.
  if ($triggering_element == 'edit-insert' or $triggering_element == "edit-edit" or $triggering_element == 'edit-add-text-content') {
    $form['level']["#value"] = trim($form['level']["#value"]);
    if ($triggering_element == 'edit-insert') {
      $levels_names = basetis_gdpr_validation_names();
      foreach ($levels_names as $key) {
        $level_name = $key->level;
        if ($level_name == $form['level']["#value"]) {
          form_set_error('level_val_ins', t('This level already exists and must be unique'));
        }
      }
      if (strlen($form['level']["#value"]) > 255) {
        form_set_error('level_long_ins', t('The level must exists and have a lenght under 255 characteres'));
      }
    }
    else {
      if ($form['level']["#value"] != $form['level']["#default_value"]) {
        $levels_names = basetis_gdpr_validation_names();
        foreach ($levels_names as $key) {
          $level_name = $key->level;
          if ($level_name == $form['level']["#value"]) {
            form_set_error('level_val_ins', t('This level already exists and must be unique'));
          }
        }
        if (strlen($form['level']["#value"]) > 255) {
          form_set_error('level_long_ed', t('The level must exists and its length must be under 255 characters'));
        }
      }
    }

    if (isset($form['description']["#value"]) and (strlen($form['description']["#value"]) > 255)) {
      form_set_error('desc_long', t('The length of the description must be under 255 characteres'));
    }
    if (isset($form['frequence']['#value']) and $form['frequence']['#value'] != '' and !is_numeric($form['frequence']["#value"])) {
      form_set_error('freq_num', t('The frequence must be numeric'));
    }
    if (strlen($form['frequence']["#value"]) > 255) {
      form_set_error('freq_long', t('The field frequence must be under 255 characters'));
    }
    if (!isset($form['frequence']["#value"]) or $form['frequence']["#value"] == '') {
      form_set_error('freq_exists', t('There must be a frequence'));
    }
  }
  // For the personalized edit or insert.
  if ($triggering_element == "edit-add") {
    $frequence = $form['new_role']['frequence_add']['#value'];
    $access_rols = $form['new_role']['access_rols_add']['#value'];
    if ($frequence == '') {
      form_set_error('freq_not_null', t("The frequence can not be empty"));
    }
    if (!($frequence == '') and !is_numeric($frequence)) {
      form_set_error('freq_type', t('The frequence must be numeric'));
    }
    if (is_numeric($frequence) and ($frequence < 0)) {
      form_set_error('freq_pos', t('The frequence must be a posistive number'));
    }
    if ($access_rols == '0') {
      form_set_error('access_rols', t('An access rol must be selected'));
    }
  }
}

/**
 * Saves form values and redirects to the content creation.
 */
function basetis_gdpr_submit_add_text_content_submit($form, &$form_state) {
  $_SESSION['basetis_gdpr_default_level_data'] = array(
    'level'         => $form['level']["#value"],
    'description'   => $form['description']["#value"],
    'frequence'     => $form['frequence']["#value"],
  );
  drupal_goto('node/add/basetis-gdpr-text-message');
}

/**
 * Called when using the filter.
 */
function basetis_gdpr_submit_buscar_submit($form, &$form_state) {
  $_SESSION['basetis_gdpr_filtre']['nom_level'] = $form['filtre']['nom_level']["#value"];
  $_SESSION['basetis_gdpr_filtre']['rol'] = $form['filtre']['rol']["#value"];
}
/**
 * Called when deleting personalized data of a sensitive level.
 *
 * Called from insert data and edit register.
 */
function submit_insert_delete_submit($form, &$form_state) {
  $name = $form_state['triggering_element']["#name"];
  $rid = explode(_, $name)[1];
  unset($_SESSION['basetis_gdpr_used_rols'][$rid]);
}

/**
 * Called when adding personalized data to a sensitive level.
 */
function submit_personalized_submit($form, &$form_state){
  $_SESSION['basetis_gdpr_default_level_data'] = array(
    'level'         => $form['level']["#value"],
    'description'   => $form['description']["#value"],
    'frequence'     => $form['frequence']["#value"],
  );

  $rid = $form['new_role']['access_rols_add']["#value"];
  $name = $form['new_role']['access_rols_add']["#options"][$rid];
  $frequence = $form['new_role']['frequence_add']["#value"];

  $_SESSION['basetis_gdpr_used_rols'][$rid] = array(
    'rid'     => $rid,
    'name'    => $name,
    'frequence'    => $frequence,
  );
}

/**
 * Deletion of the rows selected when pressing delete button.
 */
function basetis_gdpr_submit_borrar_submit($form, &$form_state) {
  $checked_ids = array();

  // It gets the selected ids that can not be deleted.
  foreach ($form_state["values"]["tabla"] as $key) {
    if ($form_state["values"]["tabla"][$key] == $key) {
      $id = $form_state["complete form"]["tabla"]["#options"][$key]["id"];
      $checked_ids[] = $id;
    }
  }
  $results = basetis_gdpr_validation_delete($form, $checked_ids);

  // If there is no element in results all columnd checked can be deleted.
  if ($results->rowcount() <= 0) {

    // It takes the content ids to delete.
    $text_message_nid = basetis_gdpr_select_text_message_nid($checked_ids);
    $nid = array();
    foreach ($text_message_nid as $key) {
      $nid[] = $key->nid;
    }

    // Si no se borra bien sale mensaje de error.
    if (basetis_gdpr_delete($form, $checked_ids) == "false") {
      drupal_set_message(t('Has been an error in the delete process'), 'error');
    }
    else {
      node_delete_multiple($nid);
    }
  }
  else {
    $niveles = "";
    foreach ($results as $result) {
      $niveles = $niveles . ', ' . $result->l_level;
    }
    if ($niveles != "") {
      $niveles = ltrim($niveles, ',');
    }
    drupal_set_message(t('Has been an error in the delete process'), 'error');
    $error_text = t('The following levels are in use:');
    $error_text = $error_text . ' ' . check_plain($niveles);
    drupal_set_message($error_text, 'error');
  }
}

/**
 * When pressing insert button redirects to insert form.
 */
function basetis_gdpr_submit_go_to_insert_submit($form, &$form_state) {
  drupal_goto('admin/gdpr/sensitive_pages/levels/add');
}

/**
 * After all necessary returns to the main page of the table.
 */
function basetis_gdpr_levels_form_submit($form, &$form_state) {
  $arg_4 = arg(4);
  if (isset($arg_4) and is_numeric($arg_4)) {
    if (basetis_gdpr_edit($form)) {
      drupal_goto('admin/gdpr/sensitive_pages/levels');
    }
    else {
      drupal_set_message(t('Has been an error in the edit process'),'error');
    }
  }
}

/**
 * Permissons are given.
 */
function basetis_gdpr_levels_permission() {
  return array(
    'access basetis gdpr levels form' => array(
      'title' => t('access basetis gdpr levels form Module'),
      'description' => t('access basetis gdpr levels form Module'),
    ),
  );
}
