CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

 INTRODUCTION
------------
The Basetis GDPR module helps you control sensitive data and makes your
site more GDPR compilant.

Allows the administrator to set diferent messages and associate them with paths
so when a user wants to enter those pages a pop-up with a message
will appear before.
Those messages and the frequence of appearance can be customized.
In order to see the page the message has to be accepted.
A log with data related to entering sensitive pages is made.

Example message:    "This page you are entering contains sensitive data and you
have to be responsible with it. Are you sure you want to continue?"

REQUIREMENTS
------------

This module requires the following modules:

 * Internacionalization (https://www.drupal.org/project/i18n)


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit:
   https://www.drupal.org/documentation/install/modules-themes/modules-7
   for further information.


CONFIGURATION
-------------

To create or modifie sensitive levels -which contains the message that
will be displayed- you can do it from admin/gdpr/sensitive_pages/levels.

To associate levels to paths with sensitive data go to gdpr/sensitive_pages.

Finally, to see the log, you can do it by entering gdpr/logs.

When installed four table are created which are:
    basetis_gdpr_sensitive_pages: stores data you set at gdpr/sensitive_pages
    basetis_gdpr_roles_levels: stores data of sensitive levels and user roles
    basetis_gdpr_page_levels: stores data of the sensitive levels created.
    basetis_gdpr_logs: stores data of interactions with sensitive pages.

When enabled, the paths mentioned will be in the admin menu in the GDPR item.
By unistalling the text messages are deleted.


MAINTAINERS
-----------

Current maintainers:
 * Zaira Ros Jimenez (ZR_basetis) - https://www.drupal.org/project/user/3610095
 * Raúl Fernández

This project has been sponsored by:
 * BaseTIS ...... Visit https://www.basetis.com for more information.
